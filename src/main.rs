use std::char;
use std::convert::TryInto;
use std::time::Instant;

use crate::compiler::compile;
use crate::instruction::Instruction;
use crate::user_input::get_file_path;

mod user_input;

mod compiler;

pub mod instruction;

fn main() {
    let now = Instant::now();
    let bf_chain: Vec<(Instruction, usize)> = compile(get_file_path());

    let mut array: Vec<i128> = vec![0; 30_000];
    let mut ptr: usize = 0;
    let mut current_char_index: usize = 0;

    while let Some((instruction, r)) = bf_chain.get(current_char_index) {
        match *instruction {
            Instruction::IncrementValue => array[ptr] += *r as i128,
            Instruction::DecrementValue => array[ptr] -= *r as i128,
            Instruction::NextPtr => {
                ptr += *r;
                if ptr >= array.len() {
                    array.resize(array.len() * 2, 0);
                }
            }
            Instruction::PreviousPtr => ptr -= *r,
            Instruction::PrintChar => {
                let char_u32: u32 = array[ptr]
                    .try_into()
                    .expect("You tried to print an impossible character.");
                let char = char::from_u32(char_u32).unwrap_or('?');
                if *r == 1 {
                    print!("{}", char)
                } else {
                    print!("{}", char.to_string().repeat(*r))
                }
            }
            Instruction::LoopStart => {
                if array[ptr] == 0 {
                    let mut count: i128 = 0;
                    while let Some((instruction, _)) = bf_chain.get(current_char_index) {
                        count += match *instruction {
                            Instruction::LoopStart => 1,
                            Instruction::LoopEnd => -1,
                            _ => 0,
                        };
                        if count == 0 {
                            break;
                        }
                        current_char_index += 1;
                    }
                }
            }
            Instruction::LoopEnd => {
                if array[ptr] != 0 {
                    let mut count: i128 = 0;
                    while let Some((instruction, _)) = bf_chain.get(current_char_index) {
                        count += match *instruction {
                            Instruction::LoopStart => 1,
                            Instruction::LoopEnd => -1,
                            _ => 0,
                        };
                        if count == 0 {
                            break;
                        }
                        current_char_index -= 1;
                    }
                }
            }
            Instruction::Comma => {
                for _ in 0..*r {
                    array[ptr] = user_input::read_char();
                }
            }
        }
        current_char_index += 1;
    }
    println!("{}s to complete !", now.elapsed().as_secs());
}
