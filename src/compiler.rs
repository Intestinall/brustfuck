use crate::instruction::Instruction;
use std::fs;
use std::process::exit;

pub fn optimize(mut data: impl Iterator<Item = Instruction>) -> Vec<(Instruction, usize)> {
    let mut v: Vec<(Instruction, usize)> = data.next().map_or(Vec::new(), |v| vec![(v, 1)]);
    let mut v_last: usize = 0;

    for instruction in data {
        if v[v_last].0 != instruction
            || matches!(v[v_last].0, Instruction::LoopStart | Instruction::LoopEnd)
        {
            v_last += 1;
            v.push((instruction, 1));
        } else {
            v[v_last].1 += 1;
        }
    }
    v
}

pub fn compile(p: String) -> Vec<(Instruction, usize)> {
    let input = match fs::read_to_string(&p) {
        Ok(v) => v,
        Err(_) => {
            println!("Cannot access '{}' : No such file or directory.", &p);
            exit(1)
        }
    };
    optimize(input.chars().filter_map(Instruction::from_char))
}
