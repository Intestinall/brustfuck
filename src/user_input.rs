use std::env;
use std::io::Read;
use std::process::exit;

pub fn get_file_path() -> String {
    match env::args().nth(1) {
        Some(v) => v,
        None => {
            println!("You must specify the path to the brainfuck file.");
            exit(1)
        }
    }
}

pub fn read_char() -> i128 {
    match std::io::stdin()
        .bytes()
        .next()
        .and_then(|result| result.ok())
        .map(|byte| byte as i128)
    {
        Some(v) => v,
        None => panic!("The input must be a valid ascii char."),
    }
}
