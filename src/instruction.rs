#[derive(PartialEq)]
pub enum Instruction {
    IncrementValue,
    DecrementValue,
    NextPtr,
    PreviousPtr,
    LoopStart,
    LoopEnd,
    PrintChar,
    Comma,
}

impl Instruction {
    pub fn from_char(c: char) -> Option<Instruction> {
        match c {
            '+' => Some(Instruction::IncrementValue),
            '-' => Some(Instruction::DecrementValue),
            '>' => Some(Instruction::NextPtr),
            '<' => Some(Instruction::PreviousPtr),
            '[' => Some(Instruction::LoopStart),
            ']' => Some(Instruction::LoopEnd),
            '.' => Some(Instruction::PrintChar),
            ',' => Some(Instruction::Comma),
            _ => None,
        }
    }
}
