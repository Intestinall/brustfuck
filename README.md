# BrustFuck
Small but fast Brainfuck interpreter in Rust.

## Usage
```
git clone https://gitlab.com/Intestinall/brustfuck.git
cargo build --release
./target/release/brustfuck <path_to_brainfuck_file>
```
